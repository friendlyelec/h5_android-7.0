#!/bin/bash

function usage() {
cat<<EOT

gen_file_verify_block.sh version "1.0"

Usage:
gen_file_verify_block.sh [-o verity_block] [-k key] [system_dir]
[system_dir] system dir for generate verity_block
[-o verity_block] output path, default is \$OUT/verity/verity_block.img
[-k key] key, default is build/tools/verity/rsa_key/rsa_key.pair
EOT
}

SYSTEM_DIR=""
OUTPUT=$OUT/verity_block.img
KEY=$ANDROID_BUILD_TOP/build/tools/verity/rsa_key/rsa_key.pair
DEBUG_INFO_DIR=""

while getopts "d:k:o:s:h" arg
do
    case $arg in
    d)
        DEBUG_INFO_DIR=$OPTARG
        ;;
    h)
        usage
        exit 0
        ;;
    k)
        KEY=$OPTARG
        ;;
    o)
        OUTPUT=$OPTARG
        ;;
    s)
        SYSTEM_DIR=$OPTARG
        ;;
    ?)
        exit 1
        ;;
    esac
done

echo "====================================="
echo "SYSTEM_DIR = $SYSTEM_DIR"
echo "KEY = $KEY"
echo "OUTPUT = $OUTPUT"
echo "DEBUG_INFO_DIR = $DEBUG_INFO_DIR"
echo "====================================="

if [ -d "$SYSTEM_DIR" ]; then
    echo "Generate file verify block for file-system $SYSTEM_DIR"
else
	echo "SYSTEM_DIR: $SYSTEM_DIR is not dir, abort..."
	exit 1
fi

FILE_SIG_TOOL=${ANDROID_BUILD_TOP}/build/tools/verity/file_sign

cd $SYSTEM_DIR
SYSTEM_DIR=$(pwd)
cd -
SYSTEM_LINK=${ANDROID_BUILD_TOP}/build/tools/verity/system
rm -rf ${SYSTEM_LINK} > /dev/null

ln -s ${SYSTEM_DIR} ${SYSTEM_LINK}
SYSTEM_DIR=${SYSTEM_LINK}

if [ "x$DEBUG_INFO_DIR" = "x" ]; then
  ${FILE_SIG_TOOL} ${SYSTEM_DIR} system sha256 ${OUTPUT} ${KEY}  >/dev/null
else
  ${FILE_SIG_TOOL} ${SYSTEM_DIR} system sha256 ${OUTPUT} ${KEY} -d ${DEBUG_INFO_DIR} >/dev/null
fi
rm -rf ${SYSTEM_LINK} > /dev/null
exit 0

