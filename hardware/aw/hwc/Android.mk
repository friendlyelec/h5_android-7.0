# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

$(warning $(TARGET_BOARD_PLATFORM))

LOCAL_PATH := $(call my-dir)

    ifneq ($(filter jaws,$(TARGET_BOARD_PLATFORM)),)
        include $(call all-named-subdir-makefiles,jaws)
    else
        ifneq ($(filter eagle,$(TARGET_BOARD_PLATFORM)),)
            include $(call all-named-subdir-makefiles,eagle)
        endif
        ifneq ($(filter dolphin,$(TARGET_BOARD_PLATFORM)),)
            include $(call all-named-subdir-makefiles,dolphin)
        endif	
        ifneq ($(filter cheetah,$(TARGET_BOARD_PLATFORM)),)
            include $(call all-named-subdir-makefiles,rabbit)
        endif
        ifneq ($(filter rabbit,$(TARGET_BOARD_PLATFORM)),)
            include $(call all-named-subdir-makefiles,rabbit)
        else
            $(warning $(TARGET_BOARD_PLATFORM))
        endif
    endif

include $(CLEAR_VARS)

