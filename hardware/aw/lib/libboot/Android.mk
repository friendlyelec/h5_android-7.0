# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_C_INCLUDES += \
	hardware/aw/include


LOCAL_SRC_FILES := \
	sunxi_boot_api.c \
	libboot_utils.c \
	libboot_para.c \
	libboot_mmc.c \
	libboot_recovery.c \

LOCAL_MODULE := libboot
LOCAL_CFLAGS += -Wall
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)


LOCAL_C_INCLUDES += \
	hardware/aw/include


LOCAL_SRC_FILES := \
	ubootmmc.c

LOCAL_STATIC_LIBRARIES := \
            libboot \
            libdtc_t \
            libc

LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_MODULE := libboot_t
LOCAL_CFLAGS += -Wall
include $(BUILD_EXECUTABLE)
